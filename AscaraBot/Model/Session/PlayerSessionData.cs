using System;
using System.Text;

namespace AscaraBot.Model.Session
{
    public class PlayerSessionData
    {
        public int Id { get; set; }
        public ulong PlayerId { get; set; }
        public DateTime LastTimePlayed { get; set;}
        public double TotalPlayTime { get; set; }
        public int LastSession { get; set; }

        public override string ToString() {
            var sb = new StringBuilder();

            sb.AppendLine($"Last time played: {LastTimePlayed}");
            sb.AppendLine($"Last session: {LastSession}");
            sb.AppendLine($"Total play time: {TotalPlayTime:F2} hours");

            return sb.ToString();
        }
    }
}