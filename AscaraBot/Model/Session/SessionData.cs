using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Discord;

namespace AscaraBot.Model.Session
{
    [Serializable]
    public enum SessionStatus
    {
        Proposed,
        Open,
        Closed,
        Started,
        Complete
    }
    
    [Serializable]
    public class SessionData
    {
        public int Id { get; set; }
        
        public SessionStatus Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? RealWorldEndDate { get; set; }
        public DateTime? EndDate { get; set; }
        public ulong Proposer { get; set; }
        public string Destination { get; set; }
        public Dictionary<ulong, string> Players { get; set; } = new Dictionary<ulong, string>();

        public TimeSpan RealDuration {
            get {
                if (!RealWorldEndDate.HasValue) return new TimeSpan(0);
                return RealWorldEndDate.Value - StartDate;
            }
        }
        
        public TimeSpan GameDuration {
            get {
                if (!EndDate.HasValue) return new TimeSpan(0);
                return EndDate.Value - StartDate;
            }
        }

        public string ShortInfoString() {
            return $"Session [{Id}] ({Status}): Starts: {StartDate}, Destination: {Destination}";
        }

        public async Task<string> GetInfoString(IGuild guild) {
            var sb = new StringBuilder();

            sb.AppendLine($"SESSION {Id} INFO:\n");

            sb.AppendLine($"Status: {Status}");

            sb.AppendLine($"Start date: {StartDate}");
            
            if (RealWorldEndDate.HasValue)
            {
                sb.AppendLine($"Real world end date: {RealWorldEndDate.Value}");
                sb.AppendLine($"Real world duration: {RealDuration.TotalHours:F2} hours");
            }
            if(EndDate.HasValue)
            {
                sb.AppendLine($"In-game end date: {EndDate.Value:d}");
                sb.AppendLine($"In-game duration: {(int) GameDuration.TotalDays} days");
            }

            var proposer = await guild.GetUserAsync(Proposer);
            sb.AppendLine($"Proposer: {proposer.Username}");

            sb.AppendLine($"Destination: {Destination}");

            sb.Append("Players: ");
            var firstAdded = false;
            foreach (var (playerId, character) in Players)           
            {
                if (firstAdded)
                {
                    sb.Append(", ");
                }
                else
                {
                    firstAdded = true;
                }

                var user = await guild.GetUserAsync(playerId);
                sb.Append($"{character} - {user.Nickname ?? user.Username}");
            }
            
            return sb.ToString();
        }
    }
}