using System.Collections.Generic;

namespace AscaraBot.Model.Inventory
{
    public class Currency
    {
        public readonly string Suffix;
        public readonly decimal ConvFactor;

        public Currency(string suffix, decimal conversionFactor)
        {
            Suffix = suffix;
            ConvFactor = conversionFactor;
        }
    }
    
    public class WalletSettings
    {
        private static WalletSettings _instance;
        public static WalletSettings Instance => _instance ?? (_instance = new WalletSettings());

        private readonly List<Currency> _currencies;
        public static List<Currency> Currencies => Instance._currencies;

        private WalletSettings()
        {
            _currencies = new List<Currency>()
            {
                new Currency("pp", 10),
                new Currency("gp", 1),
                new Currency("ep", .5m),
                new Currency("sp", .1m),
                new Currency("cp", .01m)
            };
            
            _currencies.Sort((x, y) => y.ConvFactor.CompareTo(x.ConvFactor));
        }

        public static bool IsCurrency(string text) {
            return Currencies.Find(x => x.Suffix == text) != null;
        }
    }
}