using System;
using System.Text;

namespace AscaraBot.Model.Inventory
{
    [Serializable]
    public class Wallet
    {
        public decimal CurrentMoney { get; set; }

        public Wallet()
        {
            CurrentMoney = 0;
        }

        public void AddCurrency(decimal amount, string currency)
        {
            if (string.IsNullOrEmpty(currency) || amount <= 0) throw new Exception("Invalid currency name or amount");

            var cur = WalletSettings.Currencies.Find(x => x.Suffix == currency);
            if (cur == null)
                throw new Exception($"[{currency}] is not a valid currency.");

            CurrentMoney += amount * cur.ConvFactor;
        }

        public void RemoveCurrency(decimal amount, string currency)
        {
            if (string.IsNullOrEmpty(currency) || amount <= 0) 
                throw new Exception("Invalid currency name or amount");
            
            var cur = WalletSettings.Currencies.Find(x => x.Suffix == currency);
            
            if (cur == null)
                throw new Exception($"[{currency}] is not a valid currency.");

            var correctedAmount = amount * cur.ConvFactor;
            
            if(CurrentMoney < correctedAmount)
                throw new Exception($"You don't have {amount} {currency} to remove!");

            CurrentMoney -= correctedAmount;
        }

        public string GetInfoString()
        {
            var sb = new StringBuilder();
            var accum = CurrentMoney;

            foreach (var cur in WalletSettings.Currencies)
            {
                var curAmount = Math.Floor(accum / cur.ConvFactor);

                sb.AppendLine($"{curAmount} {cur.Suffix}");
                accum -= curAmount * cur.ConvFactor;
            }

            return sb.ToString();
        }
    }
}