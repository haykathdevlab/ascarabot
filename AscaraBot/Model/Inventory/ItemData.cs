using System;

namespace AscaraBot.Model.Inventory
{
    [Serializable]
    public class ItemData
    {
        public string ItemName { get; set; }
        public int Amount { get; set; }

        public ItemData(string itemName, int amount = 0)
        {
            ItemName = itemName;
            Amount = amount;
        }

        public override string ToString()
        {
            return $"{Amount}x {ItemName}";
        }
    }
}