using System;
using System.Collections.Generic;
using System.Text;

namespace AscaraBot.Model.Inventory
{
    [Serializable]
    public class InventoryData
    {
        private static readonly string CurrencyTitleString = "--Currency--" + Environment.NewLine;
        private static readonly string ItemTitleString = "--Items--" + Environment.NewLine;
        
        public int Id { get; set; }
        public ulong OwnerId { get; set; }
        public List<ItemData> Items { get; set; }
        public Wallet Wallet { get; set; }

        public InventoryData()
        {
            Items = new List<ItemData>();
            Wallet = new Wallet();
        }
        
        public ItemData GetItem(string itemName)
        {
            return Items.Find(x => x.ItemName == itemName);
        }

        public void AddItem(string itemName, int amount)
        {
            if(string.IsNullOrWhiteSpace(itemName) || amount <= 0) return;

            var itemData = GetItem(itemName);

            if (itemData == null)
            {
                itemData = new ItemData(itemName, amount);
                Items.Add(itemData);
            }
            else
            {
                itemData.Amount += amount;
            }
        }

        public void RemoveItem(string itemName, int amount)
        {
            if (string.IsNullOrWhiteSpace(itemName) || amount <= 0)
            {
                throw new Exception("Item name must not be null and amount to remove must be greater than zero!");
            }
            
            var itemData = GetItem(itemName);

            if (itemData == null)
            {
                throw new Exception($"Item {itemName} not found!");
            }

            if (itemData.Amount < amount)
            {
                throw new Exception($"You don't have {amount} of {itemName} to remove!");
            }

            itemData.Amount -= amount;
            if (itemData.Amount <= 0)
            {
                Items.Remove(itemData);
            }
        }

        public string ListAll()
        {
            var sb = new StringBuilder();

            //Wallet contents
            sb.AppendLine(CurrencyTitleString);
            sb.Append(Wallet.GetInfoString());

            //Items
            sb.AppendLine();
            sb.AppendLine(ItemTitleString);
            foreach (var item in Items)
            {
                sb.AppendLine(item.ToString());
            }

            return sb.ToString();
        }
    }
}