using System;
using System.Threading.Tasks;
using AscaraBot.Services;
using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Yaml;
using Microsoft.Extensions.DependencyInjection;

namespace AscaraBot
{
    public class BotStarter
    {
        public IConfigurationRoot Config { get; }

        public BotStarter()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(AppContext.BaseDirectory)
                #if DEBUG
                .AddYamlFile("config_DEBUG.yml");
                #else
                .AddYamlFile("config.yml");
                #endif

            Config = builder.Build();
        }

        public static async Task StartBotAsync(string[] args)
        {
            var starter = new BotStarter();
            await starter.StartAsync();
        }

        public async Task StartAsync()
        {
            var services = new ServiceCollection();
            SetupServices(services);

            var provider = services.BuildServiceProvider();
            
            provider.GetRequiredService<CommandHandler>();
            provider.GetRequiredService<LoggingService>();
            provider.GetRequiredService<DatabaseService>();
            provider.GetRequiredService<InventoryService>();
            provider.GetRequiredService<SessionService>();
            provider.GetRequiredService<DieRollerService>();
            provider.GetRequiredService<CalcService>();

            await provider.GetRequiredService<StartupService>().StartAsync();

            await Task.Delay(-1);
        }
        
        private void SetupServices(IServiceCollection services)
        {
            services.AddSingleton(new DiscordSocketClient(new DiscordSocketConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    MessageCacheSize = 1000
                }))
                .AddSingleton(new CommandService(new CommandServiceConfig
                {
                    LogLevel = LogSeverity.Verbose,
                    DefaultRunMode = RunMode.Async
                }))
                .AddSingleton<CommandHandler>()
                .AddSingleton<StartupService>()
                .AddSingleton<LoggingService>()
                .AddSingleton<DatabaseService>()
                .AddSingleton<InventoryService>()
                .AddSingleton<SessionService>()
                .AddSingleton<DieRollerService>()
                .AddSingleton<CalcService>()
                .AddSingleton(Config);
        }
    }
}