﻿using System.Threading.Tasks;

namespace AscaraBot
{
    public static class Program
    {
        private static Task Main(string[] args) => BotStarter.StartBotAsync(args);
    }
}