using System;
using System.IO;
using System.Threading.Tasks;
using Discord;
using LiteDB;

namespace AscaraBot.Services
{
    public class DatabaseService
    {
        private const string DbFileName = "ServerData.db";

        private static string GetDbPath(IGuild guild) {
            return $"{AppDomain.CurrentDomain.BaseDirectory}{GetDbFilename(guild)}";
        }

        private static string GetDbFilename(IGuild guild) {
            return $"{guild.Id}_{DbFileName}";
        }

        public LiteDatabase GetGuildDatabase(IGuild guild) {
            return new LiteDatabase(GetDbPath(guild));
        }
        
        public async Task SendDatabaseStream(IGuild guild, IUser user) {
            var stream = File.OpenRead(GetDbPath(guild));
            await user.SendFileAsync(stream, GetDbFilename(guild));
            stream.Close();
        }
    }
}