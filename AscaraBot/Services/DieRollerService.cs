using System;
using System.Text;
using System.Text.RegularExpressions;
using Discord;

namespace AscaraBot.Services
{
    public class DieRollerService
    {
        private const string DieRollFilter = @"([0-9]+)?d([0-9]+)\s?(\+|-)?\s?([0-9]+)?";
        
        private readonly Random _rng;

        public DieRollerService() {
            _rng = new Random();
        }

        private int RollDie(int max) {
            if (max <= 0)
                return 0;
            if (max == 1)
                return 1;

            return _rng.Next(1, max + 1);
        }

        public string RollDie(string rollString, IGuildUser user) {
            var matches = Regex.Match(rollString.ToLower(), DieRollFilter);

            if (matches.Success)
            {
                var matchString = matches.Value;
                
                var dieAmount = matches.Groups[1].Success ? int.Parse(matches.Groups[1].ToString()) : 1;
                var dieValue = int.Parse(matches.Groups[2].ToString());

                var sb = new StringBuilder();

                sb.AppendLine($"{user.Nickname ?? user.Username} rolled *{matchString}*");
                sb.AppendLine("-------------");

                bool hasBonus = matches.Groups[3].Success && matches.Groups[4].Success;
                
                var resultAccum = 0;

                for (int i = 0; i < dieAmount; i++)
                {
                    var result = RollDie(dieValue);
                    resultAccum += result;
                    
                    if(dieAmount > 1 || hasBonus)
                        sb.Append($"{result} ");
                    
                    if (i != dieAmount - 1)
                        sb.Append("+ ");
                }
                
                if (hasBonus)
                {
                    if (dieAmount == 1)
                        sb.Append(" ");

                    var bonus = int.Parse(matches.Groups[4].ToString());
                    var sign = "+";
                    if (matches.Groups[3].Value == "-")
                    {
                        bonus *= -1;
                        sign = "-";
                    }

                    resultAccum += bonus;
                    sb.Append($"{sign} {Math.Abs(bonus)} ");
                }

                if (dieAmount > 1 || hasBonus)
                {
                    sb.Append("= ");
                }
                
                sb.Append($"*{resultAccum}*");

                return sb.ToString();
            }
            else
            {
                return "Invalid dice.";
            }
        }
    }
}