using System;
using System.Text;
using System.Text.RegularExpressions;
using AscaraBot.Model.Inventory;

namespace AscaraBot.Services
{
    public class CalcService
    {
        private readonly Wallet _wallet;

        private decimal _curMultiplier = 1;

        private readonly StringBuilder _sb;

        public CalcService() {
            _sb = new StringBuilder();
            _wallet = new Wallet();
        }

        public string CalcFromString(int playerCount, string text)
        {
            _sb.AppendLine("String fudida do andré:");
            _sb.AppendLine(text);
            
            _curMultiplier = 1;

            var matches = Regex.Match(text.ToLower(), @"([0-9]+)\s?([a-zA-Z]+)");

            _wallet.CurrentMoney = 0;

            _sb.AppendLine("===============");
            _sb.AppendLine("Treasure:");
            
            while (matches.Success)
            {
                string[] tokens = {matches.Groups[1].ToString(), matches.Groups[2].ToString()};

                ParseToken(tokens, 0);
                
                matches = matches.NextMatch();
            }
            
            _sb.AppendLine("===============");
            _sb.AppendLine("Total:");
            _sb.Append(_wallet.GetInfoString());

            var dividedAmount = _wallet.CurrentMoney / playerCount;
            _wallet.CurrentMoney = dividedAmount;
            
            _sb.AppendLine("===============");
            _sb.AppendLine($"Per Player ({playerCount} total):");
            _sb.AppendLine(_wallet.GetInfoString());

            var output = _sb.ToString();
            _sb.Clear();

            return output;
        }


        private void ParseToken(string[] tokens, int i)
        {
            try
            {
                decimal amount = Int32.Parse(tokens[i]);


                if (i == tokens.Length - 1)
                {
                    return;
                }
                i++;

                var unit = tokens[i];

                if (unit == "x")
                {
                    _curMultiplier = amount;
                    return;
                }

                if (WalletSettings.IsCurrency(unit))
                {
                    var total = amount * _curMultiplier;

                    if (_curMultiplier > 1)
                    {
                        _sb.Append($"{_curMultiplier} x {amount} = ");
                    }
                    _sb.Append($"{total} {unit}");
                    _sb.AppendLine();
                    
                    _curMultiplier = 1;

                    _wallet.AddCurrency(total, unit);
                }
                else
                {
                    _sb.AppendLine();
                    _curMultiplier = 1;
                }
            }
            catch
            {
                // ignored
            }
        }
    }
}