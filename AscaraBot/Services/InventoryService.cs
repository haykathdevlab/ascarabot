using System;
using System.Linq;
using System.Threading.Tasks;
using AscaraBot.Model.Inventory;
using Discord;

namespace AscaraBot.Services
{
    public class InventoryService
    {
        private readonly DatabaseService _dbService;
        
        private const ulong GuildUserId = 0;
        private const string InventoryCollectionName = "inventories";
        
        public InventoryService(DatabaseService dbService) {
            _dbService = dbService;
        }

        public string AddItem(int amount, string itemName, IGuildUser user, bool useGuildInventory = false) {
            if (WalletSettings.IsCurrency(itemName))
            {
                return AddCurrency(amount, itemName, user, useGuildInventory);
            }
            
            var inventory = useGuildInventory ? GetGuildInventory(user.Guild) : GetUserInventory(user);
            
            inventory.AddItem(itemName, amount);
            Update(inventory, user.Guild);

            var userName = user.Nickname ?? user.Username;
            var suffix = useGuildInventory ? "to the Guild's inventory" : "to their own inventory";

            return $"{userName} added {amount}x {itemName} {suffix}.";
        }

        public string AddCurrency(decimal amount, string currency, IGuildUser user, bool useGuildInventory = false) {
            var inventory = useGuildInventory ? GetGuildInventory(user.Guild) : GetUserInventory(user);
            var wallet = inventory.Wallet;

            try
            {
                wallet.AddCurrency(amount, currency);
                Update(inventory, user.Guild);

                var userName = user.Nickname ?? user.Username;
                var suffix = useGuildInventory ? "to the Guild's wallet" : "to their own wallet";

                return $"{userName} added {amount} {currency} {suffix}.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        
        public string RemoveItem(int amount, string itemName, IGuildUser user, bool useGuildInventory = false) {
            if (WalletSettings.IsCurrency(itemName))
            {
                return RemoveCurrency(amount, itemName, user, useGuildInventory);
            }
            
            var inventory = useGuildInventory ? GetGuildInventory(user.Guild) : GetUserInventory(user);

            try
            {
                inventory.RemoveItem(itemName, amount);
                Update(inventory, user.Guild);

                var userName = user.Nickname ?? user.Username;
                var suffix = useGuildInventory ? "from the Guild's inventory" : "from their own inventory";

                return $"{userName} removed {amount}x {itemName} {suffix}.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        
        public string RemoveCurrency(decimal amount, string currency, IGuildUser user, bool useGuildInventory = false) {
            var inventory = useGuildInventory ? GetGuildInventory(user.Guild) : GetUserInventory(user);
            var wallet = inventory.Wallet;

            try
            {
                wallet.RemoveCurrency(amount, currency);
                Update(inventory, user.Guild);

                var userName = user.Nickname ?? user.Username;
                var suffix = useGuildInventory ? "from the Guild's wallet" : "from their own wallet";

                return $"{userName} removed {amount} {currency} {suffix}.";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public string GetInventoryList(IGuildUser owner) {
            return $"```--{owner.Nickname ?? owner.Username}'s Inventory--\n\n{GetUserInventory(owner).ListAll()}```";
        }
        
        public string GetGuildInventoryList(IGuild guild) {
            return $"```--Guild's Inventory--\n\n{GetGuildInventory(guild).ListAll()}```";
        }
        
        private InventoryData GetUserInventory(IGuildUser user) {
            InventoryData userInv;
            using (var db = _dbService.GetGuildDatabase(user.Guild))
            {
                var col = db.GetCollection<InventoryData>(InventoryCollectionName);
                
                if (col.Exists(x => x.OwnerId == user.Id))
                {
                    userInv = col.Find(x => x.OwnerId == user.Id).First();
                }
                else
                {
                    userInv = new InventoryData
                    {
                        OwnerId = user.Id
                    };
                    col.Insert(userInv);
                }
            }

            return userInv;
        }
        
        private InventoryData GetGuildInventory(IGuild guild) {
            InventoryData guildInv;

            using (var db =_dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<InventoryData>(InventoryCollectionName);
                
                if (col.Exists(x => x.OwnerId == GuildUserId))
                {
                    guildInv = col.Find(x => x.OwnerId == GuildUserId).First();
                }
                else
                {
                    guildInv = new InventoryData
                    {
                        OwnerId = GuildUserId
                    };
                    col.Insert(guildInv);
                }
            }

            return guildInv;
        }
        
        private void Update(InventoryData invData, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<InventoryData>(InventoryCollectionName);
                col.Update(invData);
                col.EnsureIndex(x => x.OwnerId);
            }
        }
    }
}