using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AscaraBot.Model.Session;
using Discord;
using LiteDB;

//using (var db = _dbService.GetGuildDatabase(guild))
//{
//    var col = db.GetCollection<Session>(SessionCollectionName);
//}

namespace AscaraBot.Services
{
    public class SessionService
    {
        private const int MaxPlayersPerSession = 6;
        private const string SessionCollection = "sessions";
        private const string PlayerDataCollection = "playerSessionData";
        private readonly DatabaseService _dbService;

        private static readonly BsonExpression NextSessionsQuery = Query.Or(
            Query.EQ("Status", SessionStatus.Proposed.ToString()),
            Query.EQ("Status", SessionStatus.Open.ToString()),
            Query.EQ("Status", SessionStatus.Closed.ToString())
        );

        public SessionService(DatabaseService dbService) {
            _dbService = dbService;
        }

        public void ProposeSession(DateTime date, string destination, IGuildUser proposer) {
            using (var db = _dbService.GetGuildDatabase(proposer.Guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);

                var session = new SessionData
                {
                    StartDate = date,
                    Status = SessionStatus.Proposed,
                    Destination = destination,
                    Proposer = proposer.Id
                };

                col.Insert(session);
            }
        }

        public string JoinSession(int sessionId, string character, IGuildUser user, bool adminOverride = false) {
            using (var db = _dbService.GetGuildDatabase(user.Guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);

                 if (session == null || session.Status != SessionStatus.Open) 
                     throw new Exception($"Couldn't find an open session with id {sessionId}.");

                 if (session.Players.ContainsKey(user.Id))
                 {
                     var oldChar = session.Players[user.Id];
                     session.Players[user.Id] = character;
                     
                     col.Update(session);
                     return $"Changed {user.Nickname ?? user.Username}'s character in session {sessionId} from {oldChar} to {character}.";
                 }

                 if (!adminOverride && session.Players.Count >= MaxPlayersPerSession) 
                     throw new Exception("Session is full.");
                     
                 session.Players.Add(user.Id, character);

                 col.Update(session);
                 return $"{user.Nickname ?? user.Username} joined session {sessionId} with {character}.";
            }
        }
        
        public string LeaveSession(int sessionId, IGuildUser user, bool adminOverride = false) {
            using (var db = _dbService.GetGuildDatabase(user.Guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);

                if (session == null || (session.Status != SessionStatus.Open && session.Status != SessionStatus.Closed)) 
                    throw new Exception($"Couldn't find an open or closed session with id {sessionId}.");

                if (!session.Players.ContainsKey(user.Id))
                    throw new Exception("User is not in the specified session.");

                session.Players.Remove(user.Id);
                col.Update(session);
                
                return $"{user.Nickname ?? user.Username} has left session {sessionId}.";
            }
        }

        public string ChangeSessionStartTime(int sessionId, DateTime? date, TimeSpan? time, IGuildUser user) {
            using (var db = _dbService.GetGuildDatabase(user.Guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);

                if (session == null || session.Status > SessionStatus.Closed)
                    throw new Exception($"Couldn't find an open or closed session with id {sessionId}.");

                var isAdmin = user.GuildPermissions.Administrator;
                var isProposer = session.Proposer == user.Id;

                if (isProposer && session.Status > SessionStatus.Open && !isAdmin)
                {
                    throw new Exception("Only an admin can change the start time of a closed session!");
                }
                
                if (!isProposer && !isAdmin)
                {
                    throw new Exception("You can only change a session's start time if you're an admin or the session's proposer!");
                }

                if (time.HasValue)
                {
                    session.StartDate = session.StartDate.Date + time.Value;
                }
                else if (date.HasValue)
                {
                    session.StartDate = date.Value + session.StartDate.TimeOfDay;
                }

                col.Update(session);
                
                return $"Changed the session start time for session {sessionId}. It will start at {session.StartDate}.";
            }
        }

        public string OpenSession(int sessionId, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);
                
                if (session == null)
                    throw new Exception($"Couldn't find session {sessionId}!");

                if (session.Status != SessionStatus.Proposed)
                    throw new Exception($"Session {sessionId} isn't proposed anymore!");

                session.Status = SessionStatus.Open;
                col.Update(session);

                return
                    $"Session {sessionId} towards {session.Destination} on {session.StartDate} is now open. Come join!";
            }
        }
        
        public string CloseSession(int sessionId, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);
                
                if (session == null)
                    throw new Exception($"Couldn't find session {sessionId}!");

                if (session.Status != SessionStatus.Open)
                    throw new Exception($"Session {sessionId} isn't open anymore!");

                session.Status = SessionStatus.Closed;
                col.Update(session);

                return
                    $"Session {sessionId} towards {session.Destination} on {session.StartDate} is now closed. See you when it starts!";
            }
        }
        
        public string CancelSession(int sessionId, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);

                if (session == null)
                    throw new Exception($"Couldn't find session {sessionId}!");

                if (session.Status > SessionStatus.Started)
                    throw new Exception($"Session {sessionId} is already over!");

                col.Delete(sessionId);
                
                return
                    $"Session {sessionId} towards {session.Destination} has been canceled by the DM. Too bad!";
            }
        }
        
        public string StartSession(int sessionId, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                var session = col.FindById(sessionId);

                if (session == null)
                    throw new Exception($"Couldn't find session {sessionId}!");

                if (session.Status != SessionStatus.Open && session.Status != SessionStatus.Closed)
                    throw new Exception($"Session {sessionId} isn't open or closed!");

                session.StartDate = DateTime.Now;
                session.Status = SessionStatus.Started;
                col.Update(session);

                return
                    $"Session {sessionId} towards {session.Destination} has started! Good luck!";
            }
        }
        
        public string EndSession(int sessionId, DateTime endDate, IGuild guild) {
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var sessionCol = db.GetCollection<SessionData>(SessionCollection);
                var playerCol = db.GetCollection<PlayerSessionData>(PlayerDataCollection);
                
                var session = sessionCol.FindById(sessionId);

                if (session == null)
                    throw new Exception($"Couldn't find session {sessionId}!");

                if (session.Status != SessionStatus.Started)
                    throw new Exception($"Session {sessionId} hasn't started yet!");
                
                session.EndDate = endDate;
                session.RealWorldEndDate = DateTime.Now;
                session.Status = SessionStatus.Complete;
                sessionCol.Update(session);
                
                var playersSb = new StringBuilder();
                var firstAdded = false;
                foreach (var (playerId, character) in session.Players)
                {
                    if (firstAdded)
                    {
                        playersSb.Append(", ");
                    }
                    else
                    {
                        firstAdded = true;
                    }

                    CreateOrUpdatePlayerData(playerCol, playerId, session);

                    playersSb.Append(character);
                }

                return
                    $"Session {sessionId} towards {session.Destination} has ended! {playersSb} will return to Ascarã on {endDate:d}.";
            }
        }

        public string ListNextSessions(IGuild guild) {
            SessionData[] sessions;
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);

                sessions = col.Find(NextSessionsQuery).ToArray();
            }

            if (sessions.Length == 0) return "No open sessions found!";

            var sb = new StringBuilder();
            foreach (var session in sessions)
            {
                sb.AppendLine(session.ShortInfoString());
            }

            return sb.ToString();
        }

        public string GetPlayerInfo(IGuildUser user) {
            using (var db = _dbService.GetGuildDatabase(user.Guild))
            {
                var playerCol = db.GetCollection<PlayerSessionData>(PlayerDataCollection);

                var playerData = playerCol.FindOne(x => x.PlayerId == user.Id);

                if (playerData == null) 
                    throw new Exception($"No session data found for player: {user.Nickname ?? user.Username}");
                
                return $"{user.Nickname ?? user.Username}'s session info:\n\n{playerData}";
            }
        }

        public async Task<string> GetSessionInfo(int id, IGuild guild) {
            SessionData sessionData;
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);
                sessionData = col.FindById(id);
            }

            if (sessionData == null) 
                throw new Exception($"Couldn't find session {id}!");

            var info = await sessionData.GetInfoString(guild);
            return info;
        }

        public async Task<string> ListAllSessions(IGuild guild) {
            SessionData[] sessions;
            using (var db = _dbService.GetGuildDatabase(guild))
            {
                var col = db.GetCollection<SessionData>(SessionCollection);

                sessions = col.FindAll().ToArray();
            }

            if (sessions.Length <= 0) return "No sessions found!";

            var sb = new StringBuilder();
            foreach (var session in sessions)
            {
                var sessionInfo = await session.GetInfoString(guild);
                sb.AppendLine(sessionInfo);
                sb.AppendLine("\n--------------");
            }

            return sb.ToString();
        }
        
        private static void CreateOrUpdatePlayerData(ILiteCollection<PlayerSessionData> playerCol, ulong playerId, SessionData session) {
            var playerData = playerCol.FindOne(x => x.PlayerId == playerId);
            if (playerData == null)
            {
                playerData = new PlayerSessionData
                {
                    PlayerId = playerId,
                    LastTimePlayed = session.StartDate,
                    LastSession = session.Id,
                    TotalPlayTime = session.RealDuration.TotalHours
                };
                playerCol.Insert(playerData);
            }
            else
            {
                playerData.LastTimePlayed = session.StartDate;
                playerData.LastSession = session.Id;
                playerData.TotalPlayTime += session.RealDuration.TotalHours;
                playerCol.Update(playerData);
            }
        }
    }
}