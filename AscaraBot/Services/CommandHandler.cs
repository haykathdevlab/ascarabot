using System;
using System.Threading.Tasks;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;

namespace AscaraBot.Services
{
    public class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _commands;
        private readonly IConfigurationRoot _config;
        private readonly IServiceProvider _provider;
        

        public CommandHandler(DiscordSocketClient client, CommandService commands, IConfigurationRoot config, IServiceProvider provider)
        {
            _client = client;
            _commands = commands;
            _config = config;
            _provider = provider;

            _client.MessageReceived += HandleCommandAsync;
        }

        private async Task HandleCommandAsync(SocketMessage msgParam)
        {
            if (!(msgParam is SocketUserMessage msg)) return;
            if (msg.Author.Id == _client.CurrentUser.Id) return;

            var argPos = 0;

            if (msg.HasStringPrefix(_config["commandPrefix"], ref argPos) ||
                msg.HasMentionPrefix(_client.CurrentUser, ref argPos))
            {
                var context = new SocketCommandContext(_client, msg);
                
                var result = await _commands.ExecuteAsync(context, argPos, _provider);

                if (result.IsSuccess)
                {
                    await msg.DeleteAsync();
                }
                else
                {
                    await context.Channel.SendMessageAsync(result.ToString());
                }
            }
        }
    }
}