using System.Threading.Tasks;
using AscaraBot.Model.Inventory;
using AscaraBot.Services;
using Discord;
using Discord.Commands;

namespace AscaraBot.Modules
{
    [Group("list")]
    public class ListModule : ModuleBase<SocketCommandContext>
    {
        private readonly InventoryService _inventories;

        public ListModule(InventoryService inventories)
        {
            _inventories = inventories;
        }

        [Command, Summary("Lists the items in your inventory")]
        public async Task ListCommandAsync()
        {
            await ReplyAsync(_inventories.GetInventoryList(Context.User as IGuildUser));
        }
        
        [Command, Summary("Lists the items in a users inventory")]
        public async Task ListCommandAsync(IGuildUser user)
        {
            await ReplyAsync(_inventories.GetInventoryList(user));
        }
        
        [Command("guild"), Summary("Lists the items in the guild's inventory")]
        [Alias("g")]
        public async Task ListGuildCommandAsync()
        {
            await ReplyAsync(_inventories.GetGuildInventoryList(Context.Guild));
        }

    }
}