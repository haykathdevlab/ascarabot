using System.Threading.Tasks;
using AscaraBot.Services;
using Discord.Commands;

namespace AscaraBot.Modules
{
    public class CalcModule : ModuleBase<SocketCommandContext>
    {
        private readonly CalcService _calc;

        public CalcModule(CalcService calc) {
            _calc = calc;
        }
        
        [Command("calc"), Summary("Calculates weird treasure strings")]
        [Alias("c")]
        public async Task Calculate(int playerCount, [Remainder] string treasure) {
            var result = _calc.CalcFromString(playerCount, treasure);
            await ReplyAsync($"```{result}```");
        }
    }
}