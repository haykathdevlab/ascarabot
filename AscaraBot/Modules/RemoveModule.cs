using System;
using System.Threading.Tasks;
using AscaraBot.Model.Inventory;
using AscaraBot.Services;
using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace AscaraBot.Modules
{
    [Group("remove")]
    public class RemoveModule : ModuleBase<SocketCommandContext>
    {
        private readonly InventoryService _inventories;

        public RemoveModule(InventoryService inventories)
        {
            _inventories = inventories;
        }

        [Command, Summary("Removes one of an item from the users inventory")]
        public async Task RemoveItemCommand([Remainder] string itemName) {
            await ReplyAsync(_inventories.RemoveItem(1, itemName, Context.User as IGuildUser));
        }
        
        
        [Command, Summary("Removes a given amount of an item from the users inventory")]
        public async Task RemoveItemCommand(int amount, [Remainder] string itemName)
        {
            await ReplyAsync(_inventories.RemoveItem(amount, itemName, Context.User as IGuildUser));
        }
        
        //Currency
        [Command("currency"), Summary("Removes an amount of currency from the users inventory")]
        [Alias("c")]
        public async Task RemoveCurrencyCommand(decimal amount, [Remainder] string currencySuffix) {
            await ReplyAsync(_inventories.RemoveCurrency(amount, currencySuffix, Context.User as IGuildUser));
        }
        
        
        [Command("guild"), Summary("Removes an item from the guild's inventory")]
        [Alias("g")]
        public async Task RemoveGuildCommandAsync([Remainder] string itemName)
        {
            await ReplyAsync(_inventories.RemoveItem(1, itemName, Context.User as IGuildUser, true));
        }

        [Command("guild"), Summary("Removes an item from the guild's inventory")]
        [Alias("g")]
        public async Task RemoveGuildCommandAsync(int amount, [Remainder] string itemName)
        {
            await ReplyAsync(_inventories.RemoveItem(amount, itemName, Context.User as IGuildUser, true));
        }

        [Command("guild currency"), Summary("Removes currency from the guild inventory")]
        [Alias("g c", "guild c", "g currency")]
        public async Task RemoveGuildCurrencyCommand(decimal amount, [Remainder] string currencySuffix)
        {
            await ReplyAsync(_inventories.RemoveCurrency(amount, currencySuffix, Context.User as IGuildUser, true));
        }
    }
}