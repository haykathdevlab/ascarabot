using System.Threading.Tasks;
using AscaraBot.Services;
using Discord;
using Discord.Commands;

namespace AscaraBot.Modules
{
    public class DieRollModule : ModuleBase<SocketCommandContext>
    {
        private DieRollerService _dieRoller;

        public DieRollModule(DieRollerService dieRoller) {
            _dieRoller = dieRoller;
        }

        [Command("roll"), Summary("Rolls some dice")]
        [Alias("r")]
        public async Task Roll([Remainder] string die) {
            var result = _dieRoller.RollDie(die, Context.User as IGuildUser);
            await ReplyAsync($"```{result}```");
        }
    }
}