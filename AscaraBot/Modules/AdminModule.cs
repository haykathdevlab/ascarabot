using System.Threading.Tasks;
using AscaraBot.Services;
using Discord;
using Discord.Commands;

namespace AscaraBot.Modules
{
    public class AdminModule : ModuleBase<SocketCommandContext>
    {
        private readonly DatabaseService _dbService;
        private readonly InventoryService _invService;
        
        public AdminModule(DatabaseService dbService, InventoryService invService) {
            _dbService = dbService;
            _invService = invService;
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [RequireOwner(Group = "Permission")]
        [Command("backup")]
        [Summary("Sends a database backup as dm to the caller.")]
        public async Task BackupCommand() {
            await _dbService.SendDatabaseStream(Context.Guild, Context.User);
        }
        
        [RequireOwner(Group = "Permission")]
        [Command("convert")]
        [Summary("Do not use this.")]
        public async Task ConvertCommand() {
            await ReplyAsync("I told you not to do this!.");
        }
    }
}