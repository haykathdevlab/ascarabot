using System;
using System.Threading.Tasks;
using AscaraBot.Model.Inventory;
using AscaraBot.Services;
using Discord;
using Discord.Commands;

namespace AscaraBot.Modules
{
    [Group("add")]
    public class AddModule : ModuleBase<SocketCommandContext>
    {
        private readonly InventoryService _inventories;

        public AddModule(InventoryService inventories)
        {
            _inventories = inventories;
        }

        [Command, Summary("Adds an item to your own inventory")]
        public async Task AddCommandAsync([Remainder] string itemName) {
            await ReplyAsync(_inventories.AddItem(1, itemName, Context.User as IGuildUser));
        }

        [Command, Summary("Adds an item to your own inventory")]
        public async Task AddCommandAsync(int amount, [Remainder] string itemName)
        {
            await ReplyAsync(_inventories.AddItem(amount, itemName, Context.User as IGuildUser));
        }

        [Command("currency"), Summary("Adds currency to your inventory")]
        [Alias("c")]
        public async Task CurrencyCommandAsync(decimal amount, string currencyPrefix) {
            await ReplyAsync(_inventories.AddCurrency(amount, currencyPrefix, Context.User as IGuildUser));
        }
        
        [Command("guild"), Summary("Adds an item to the guild's inventory")]
        [Alias("g")]
        public async Task AddGuildCommandAsync([Remainder] string itemName)
        {
            await ReplyAsync(_inventories.AddItem(1, itemName, Context.User as IGuildUser, true));
        }

        [Command("guild"), Summary("Adds an item to the guild's inventory")]
        [Alias("g")]
        public async Task AddGuildCommandAsync(int amount, [Remainder] string itemName)
        {
            await ReplyAsync(_inventories.AddItem(amount, itemName, Context.User as IGuildUser,true));
        }

        [Command("guild currency"), Summary("Adds currency to the guild inventory")]
        [Alias("g c", "guild c", "g currency")]
        public async Task CurrencyGuildCommandAsync(decimal amount, string currencyPrefix)
        {
            await ReplyAsync(_inventories.AddCurrency(amount, currencyPrefix, Context.User as IGuildUser, true));
        }
    }
}