using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using AscaraBot.Services;
using Discord;
using Discord.Commands;

namespace AscaraBot.Modules
{
    [Group("session")]
    [Alias("s")]
    public class SessionModule : ModuleBase<SocketCommandContext>
    {
        private readonly SessionService _sessionService;

        public SessionModule(SessionService sessionService) {
            _sessionService = sessionService;
        }

        #region Control
        
        [Command("propose")]
        [Alias("p")]
        public async Task ProposeCommand(DateTime date, [Remainder] string destination) {
            _sessionService.ProposeSession(date, destination, Context.User as IGuildUser);
            await ReplyAsync($"{Context.User.Username} proposed a session to {destination} on {date}.");
        }

        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("open")]
        [Alias("o")]
        public async Task OpenCommand(int sessionId) {
            try
            {
                await ReplyAsync(_sessionService.OpenSession(sessionId, Context.Guild));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("close")]
        [Alias("cl")]
        public async Task CloseCommand(int sessionId) {
            try
            {
                await ReplyAsync(_sessionService.CloseSession(sessionId, Context.Guild));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("cancel")]
        [Alias("cn")]
        public async Task CancelCommand(int sessionId) {
            try
            {
                await ReplyAsync(_sessionService.CancelSession(sessionId, Context.Guild));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("start")]
        [Alias("s")]
        public async Task StartCommand(int sessionId) {
            try
            {
                await ReplyAsync(_sessionService.StartSession(sessionId, Context.Guild));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("end")]
        [Alias("e")]
        public async Task EndCommand(int sessionId, DateTime date) {
            try
            {
                await ReplyAsync(_sessionService.EndSession(sessionId, date, Context.Guild));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }

        #endregion

        #region Join/Leave

        [Command("join")]
        [Alias("j")]
        public async Task JoinCommand(int id, [Remainder] string character) {
            try
            {
                await ReplyAsync(_sessionService.JoinSession(id, character, Context.User as IGuildUser));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("join")]
        [Alias("j")]
        public async Task JoinCommand(int id, IGuildUser user, [Remainder] string character) {
            try
            {
                await ReplyAsync(_sessionService.JoinSession(id, character, user, true));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [Command("leave")]
        [Alias("lv")]
        public async Task LeaveCommand(int id) {
            try
            {
                await ReplyAsync(_sessionService.LeaveSession(id, Context.User as IGuildUser));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [RequireUserPermission(GuildPermission.Administrator, Group = "Permission")]
        [Command("leave")]
        [Alias("lv")]
        public async Task LeaveCommand(int id, IGuildUser user) {
            try
            {
                await ReplyAsync(_sessionService.LeaveSession(id, user, true));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }

        #endregion

        #region Date/Time

        [Command("time")]
        [Alias("t")]
        public async Task TimeCommand(int id, TimeSpan time) {
            try
            {
                await ReplyAsync(_sessionService.ChangeSessionStartTime(id, null, time, Context.User as IGuildUser));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [Command("date")]
        [Alias("d")]
        public async Task DateCommand(int id, DateTime date) {
            try
            {
                await ReplyAsync(_sessionService.ChangeSessionStartTime(id, date, null, Context.User as IGuildUser));
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }

        #endregion

        #region Info

        [Command("list")]
        [Alias("ls")]
        public async Task ListOpenCommand() {
            var list = _sessionService.ListNextSessions(Context.Guild);
            await ReplyAsync($"```{list}```");
        }

        [Command("info")]
        [Alias("i")]
        public async Task InfoCommand(int sessionId) {
            var info = await _sessionService.GetSessionInfo(sessionId, Context.Guild);
            await ReplyAsync($"```{info}```");
        }
        
        [Command("info")]
        [Alias("i")]
        public async Task InfoCommand(IGuildUser user) {
            try
            {
                await ReplyAsync($"```{_sessionService.GetPlayerInfo(user)}```");
            }
            catch (Exception e)
            {
                await ReplyAsync(e.Message);
            }
        }
        
        [Command("history")]
        [Alias("h")]
        public async Task ListAllCommand() {
            var info = await _sessionService.ListAllSessions(Context.Guild);
            var bytes = Encoding.UTF8.GetBytes(info);
            var stream = new MemoryStream(bytes);
            
            await Context.User.SendFileAsync(stream, "sessionHistory.txt");
        }
        
        #endregion
    }
}