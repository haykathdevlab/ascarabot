using System.Threading.Tasks;
using Discord.Commands;

namespace AscaraBot.Modules
{
    public class TestModule : ModuleBase<SocketCommandContext>
    {
        [Command("say")]
        [Summary("Sends a message with the given text")]
        public Task SayCommand([Remainder] [Summary("The text to be sent")] string msg) => ReplyAsync(msg);
    }
}