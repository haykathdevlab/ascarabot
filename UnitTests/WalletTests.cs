using AscaraBot.Model.Inventory;
using NUnit.Framework;

namespace UnitTests
{
    public class WalletTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void AddCurrencyTest()
        {
            var wal = new Wallet();
            wal.AddCurrency(10, "gp");
            Assert.AreEqual(10, wal.CurrentMoney);
        }

        [Test]
        public void AddAllCurrenciesTest()
        {
            foreach (var cur in WalletSettings.Currencies)
            {
                var wal = new Wallet();
                wal.AddCurrency(1000, cur.Suffix);
                
                Assert.AreEqual(1000 * cur.ConvFactor, wal.CurrentMoney);
            }
        }

        [Test]
        public void AddNonExistantCurrencyTest()
        {
            var wal = new Wallet();
            Assert.Catch(() => wal.AddCurrency(10, "pintos"));
        }

        [Test]
        public void InfoStringTest()
        {
            var currency = 55.75m; //5 pp + 5 gp + 1 ep + 2 sp + 5 cp
            var expectedOutput = "5 pp\r\n" +
                                 "5 gp\r\n" +
                                 "1 ep\r\n" +
                                 "2 sp\r\n" +
                                 "5 cp\r\n";
            
            var wal = new Wallet();
            wal.AddCurrency(currency, "gp");
            
            Assert.AreEqual(expectedOutput, wal.GetInfoString());
        }
    }
}